package com.example.apiorders.model;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(value = "order")
@Data

public class Order {
    @Id
    private String id;
    private double finalPrice;
    private String state;

}
