package com.example.apiorders.model;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(value = "order-detail")
@Data

public class OrderDetail {
    @Id
    private String id;
    private String order;
    private String product;
    private double quantity;
    private double price;
    private boolean state;

}
