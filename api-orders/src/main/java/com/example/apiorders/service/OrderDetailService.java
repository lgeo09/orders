package com.example.apiorders.service;

import com.example.apiorders.model.OrderDetail;
import com.example.apiorders.repository.OrderDetailRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class OrderDetailService {

    private final OrderDetailRepository orderDetailRepository;

    public void save(OrderDetail orderDetail) { orderDetailRepository.save(orderDetail); }

    public List<OrderDetail> findAll() { return orderDetailRepository.findAll(); }

    public Optional<OrderDetail> findById(String id) {return orderDetailRepository.findById(id); }

    public void deleteById(String id) { orderDetailRepository.deleteById(id); }

}
