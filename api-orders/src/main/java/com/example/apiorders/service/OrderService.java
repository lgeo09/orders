package com.example.apiorders.service;

import com.example.apiorders.model.Order;
import com.example.apiorders.repository.OrderRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class OrderService {

    private final OrderRepository orderRepository;

    public void save (Order order) { orderRepository.save(order); }

    public List<Order> findAll() { return orderRepository.findAll(); }

    public Optional<Order> findById(String id) { return orderRepository.findById(id); }

    public void deleteById(String id) { orderRepository.deleteById(id); }
}
