package com.example.apiorders.controller;

import com.example.apiorders.model.Order;
import com.example.apiorders.service.OrderService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api")
@RequiredArgsConstructor
public class OrderController {

    private final OrderService orderService;

    @PostMapping("/orders")
    public void save(@RequestBody Order order) { orderService.save(order);}

    @GetMapping("/orders")
    public List<Order> findAll(){
        return orderService.findAll();
    }

    @GetMapping("/orders/{id}")
    public Order findById(@PathVariable String id) {
        return orderService.findById(id).get();
    }

    @DeleteMapping("/orders/{id}")
    public void deleteById(@PathVariable String id){
        orderService.deleteById(id);
    }

    @PutMapping("/orders")
    public void update(@RequestBody Order order) {
        orderService.save(order);
    }

}
