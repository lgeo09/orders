package com.example.apiorders.controller;

import com.example.apiorders.model.OrderDetail;
import com.example.apiorders.model.Product;
import com.example.apiorders.service.OrderDetailService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api")
@RequiredArgsConstructor
public class OrderDetailController {
    private final OrderDetailService orderDetailService;

    @PostMapping("/ordersDetails")
    public void save(@RequestBody OrderDetail orderDetail) {
        orderDetailService.save(orderDetail);
    }

    @GetMapping("/ordersDetails")
    public List<OrderDetail> findAll(){
        return orderDetailService.findAll();
    }

    @GetMapping("/ordersDetails/{id}")
    public OrderDetail findById(@PathVariable String id) {
        return orderDetailService.findById(id).get();
    }

    @DeleteMapping("/ordersDetails/{id}")
    public void deleteById(@PathVariable String id){
        orderDetailService.deleteById(id);
    }

    @PutMapping("/ordersDetails")
    public void update(@RequestBody OrderDetail orderDetail) {
        orderDetailService.save(orderDetail);
    }

}
