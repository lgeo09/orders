package com.example.apiorders.controller;

import com.example.apiorders.model.Product;
import com.example.apiorders.service.ProductService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api")
@RequiredArgsConstructor
public class ProductController {

    private final ProductService productService;

    @CrossOrigin(origins = "http://localhost:3000")
    @PostMapping("/products")
    public void save(@RequestBody Product product) {
        productService.save(product);
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @GetMapping("/products")
    public List<Product> findAll(){
       return productService.findAll();
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @GetMapping("/products/{id}")
    public Product findById(@PathVariable String id) {
        return productService.findById(id).get();
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @DeleteMapping("/products/{id}")
    public void deleteById(@PathVariable String id){
        productService.deleteById(id);
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @PutMapping("/products")
    public void update(@RequestBody Product product) {
        productService.save(product);
    }

}
